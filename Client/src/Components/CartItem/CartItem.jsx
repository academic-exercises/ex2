import useStyles from "./styles";
import { Grid } from "@mui/material";
import { AddCommas } from "../../Utils/General";
import Button from "react-bootstrap/Button";
import { CartContext } from "../../Context/CartContext";
import { useContext } from "react";

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

const CartItem = ({ item, index }) => {
  const classes = useStyles();
  const { cartItems, setCartItems } = useContext(CartContext);

  return (
    <Grid
      container
      alignItems="center"
      style={{
        padding: 12,
        margin: 12,
        borderRadius: 12,
      }}
    >
      <Grid item xs={6}>
        <h3 className={classes.text}>{capitalizeFirstLetter(item.title)}</h3>
        <h6 className={classes.text}>{item.description}</h6>
        <h6 className={classes.text}>{AddCommas(item.price)}$</h6>
        <Button
          className={classes.removeFromCart}
          onClick={() => {
            setCartItems(
              cartItems.filter((cartItem, filterIndex) => filterIndex !== index)
            );
          }}
        >
          REMOVE FROM CART
        </Button>
      </Grid>
      <Grid item xs={6}>
        <img className={classes.image} src={item.imageURL} alt={item.name} />
      </Grid>
    </Grid>
  );
};

export default CartItem;
