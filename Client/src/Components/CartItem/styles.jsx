import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
  root: {
    height: "100vh",
  },
  text: {
    textAlign: "left",
  },
  image: {
    width: "100%",
    maxHeight: "36vh",
    objectFit: "cover",
    maxWidth: "100%",
  },
  removeFromCart: {
    backgroundColor: "#d11a2a",
    borderRadius: 0,
    border: "none",
    padding: "12px 20px",
    margin: 12,
    fontWeight: "bold",
    fontSize: "1.2rem",
    cursor: "pointer",

    "&:hover": {
      backgroundColor: "rgb(229, 26, 42)",
      color: "white",
    },
  },
});

export default useStyles;
