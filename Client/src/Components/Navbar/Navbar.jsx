import useStyles from "./styles";
import { Grid, Typography } from "@mui/material";
import { AddCommas } from "../../Utils/General";
import { useNavigate } from "react-router-dom";
import { Badge, Button } from "react-bootstrap";
import CartManager from "../../Utils/CartManager";
import { CartContext } from "../../Context/CartContext";
import { useContext } from "react";

const Navbar = () => {
  const classes = useStyles();
  const navigate = useNavigate();
  const { cartItems } = useContext(CartContext);

  return (
    <Grid
      className={classes.root}
      container
      justifyContent="center"
      alignItems="center"
    >
      <Grid item xs={10}>
        <Grid className={classes.root} container alignItems="center">
          <Grid item xs={2}>
            <Typography
              className={classes.title}
              variant="h5"
              onClick={() => navigate("/")}
            >
              Clothes Store
            </Typography>
          </Grid>
          <div style={{ flexGrow: 1 }} />

          <Grid item xs={2}>
            <Button
              variant="light"
              onClick={() => {
                navigate("/cart");
              }}
            >
              Cart <Badge bg="secondary">{cartItems.length}</Badge>
              <span className="visually-hidden">unread messages</span>
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Navbar;
