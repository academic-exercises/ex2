import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
  root: {
    width: "100vw",
    backgroundColor: "#f5f5f5",
  },
  title: {
    textAlign: "left",
    color: "black",
    padding: "0.5rem",
  },

  badge: {
    backgroundColor: "rgb(214, 0, 1)",
    color: "white",
    fontWeight: "bold",
    fontSize: "1.2rem",
    padding: "0.5rem",
    borderRadius: 12,
  },
});

export default useStyles;
