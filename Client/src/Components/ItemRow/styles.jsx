import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
  root: {
    height: "100vh",
  },
  text: {
    textAlign: "left",
  },
  image: {
    width: "100%",
    maxHeight: "36vh",
    objectFit: "cover",
    maxWidth: "100%",
  },
  addToCart: {
    backgroundColor: "#018849",
    borderRadius: 0,
    border: "none",
    padding: "12px 70px",
    margin: 12,
    fontWeight: "bold",
    fontSize: "1.2rem",
    cursor: "pointer",

    "&:hover": {
      backgroundColor: "#006637",
      color: "white",
    },
  },
});

export default useStyles;
