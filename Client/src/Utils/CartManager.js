const cartsItems = [];

const getCartsItems = () => {
  return cartsItems;
};

const addCartItem = (item) => {
  cartsItems.push(item);
};

const CartManager = {
  getCartsItems,
  addCartItem,
};

export default CartManager;
