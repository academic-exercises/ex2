import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
  root: {},
  title: {
    textAlign: "center",
    paddingTop: "2rem",
  },
  image: {
    objectFit: "contain",
    borderRadius: 12,
    maxHeight: "30vh",
  },
  checkoutButton: {
    backgroundColor: "#018849",
    width: "100%",
    borderRadius: 0,
    border: "none",

    "&:hover": {
      backgroundColor: "#006637",
      color: "white",
    },

    "&:disabled": {
      backgroundColor: "#6c757d",
      color: "white",
    },
  },
});

export default useStyles;
