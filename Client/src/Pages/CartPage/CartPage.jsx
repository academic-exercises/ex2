import useStyles from "./styles";
import { Grid, Typography } from "@mui/material";

import { CartContext } from "../../Context/CartContext";
import { useContext, useState } from "react";
import CartItem from "../../Components/CartItem/CartItem";
import { Alert, Button } from "react-bootstrap";

const CartPage = () => {
  const classes = useStyles();
  const [showAlert, setShowAlert] = useState(false);
  const { cartItems, setCartItems } = useContext(CartContext);

  const sendingCartToServer = async () => {
    const cartItemsWithoutId = cartItems.map((item) => {
      return {
        name: item.name,
        price: item.price,
        description: item.description,
        imageURL: item.imageURL,
      };
    });

    await fetch(`${window.SERVER_URL}/cart`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(cartItemsWithoutId),
    });
    setShowAlert(true);
    setCartItems([]);

    setTimeout(() => {
      setShowAlert(false);
    }, 3000);
  };

  return (
    <Grid
      className={classes.root}
      container
      justifyContent="start"
      alignItems="start"
    >
      <Grid item xs={12} className={classes.title}>
        <Typography variant="h2">Your Cart</Typography>
      </Grid>

      <Grid item xs={10}>
        <Grid container justifyContent="center" alignItems="start" spacing={2}>
          <Grid item xs={5} className={classes.title}>
            <Grid
              container
              justifyContent="start"
              alignItems="start"
              spacing={2}
            >
              {cartItems.map((item, index) => (
                <CartItem key={index} item={item} index={index} />
              ))}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={1}>
        <Grid container justifyContent="start" alignItems="start" spacing={2}>
          <Grid item xs={12}>
            <Typography variant="h5">
              Total:{" "}
              {cartItems
                .map((item) => item.price)
                .reduce((partialSum, a) => partialSum + a, 0)}
              $
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="h5">
              Total Items: {cartItems.length}
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Button
              className={classes.checkoutButton}
              variant="primary"
              onClick={() => {
                sendingCartToServer();
              }}
              disabled={cartItems.length === 0}
            >
              Send
            </Button>
          </Grid>
          <Grid item xs={12}>
            {showAlert && (
              <Alert variant="success">Your Purchased Sent Successfully</Alert>
            )}
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default CartPage;
