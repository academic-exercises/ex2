import useStyles from "./styles";
import { Grid } from "@mui/material";

import ItemRow from "../../Components/ItemRow/ItemRow";
import { useQuery } from "react-query";

const HomePage = () => {
  const classes = useStyles();
  const { isLoading, error, data } = useQuery("storeData", () =>
    fetch(`${window.SERVER_URL}/store`).then((res) => res.json())
  );

  return (
    <Grid
      className={classes.root}
      container
      justifyContent="center"
      alignItems="start"
    >
      <Grid item xs={5} className={classes.title}>
        <Grid container justifyContent="center" alignItems="start" spacing={2}>
          {!isLoading &&
            data.map((item, index) => (
              <ItemRow key={index} item={item} index={index} />
            ))}
        </Grid>
      </Grid>
    </Grid>
  );
};

export default HomePage;
