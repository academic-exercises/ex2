import { Route, Routes } from "react-router-dom";
import { QueryClient, QueryClientProvider } from "react-query";

const queryClient = new QueryClient();
import HomePage from "./Pages/HomePage/HomePage";
import Navbar from "./Components/Navbar/Navbar";
import { CartContextWrapper } from "./Context/CartContext";
import CartPage from "./Pages/CartPage/CartPage";

const App = () => {
  return (
    <CartContextWrapper>
      <QueryClientProvider client={queryClient}>
        <Navbar />
        <Routes>
          <Route path="/" caseSensitive={false} element={<HomePage />} />
          <Route path="/cart" element={<CartPage />} />
        </Routes>
      </QueryClientProvider>
    </CartContextWrapper>
  );
};

export default App;
