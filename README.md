# ex2

## How to run this project?

The order of this steps is important

### 1. install node modules

You need to install node modules for both `Server` and `Client` by running `npm i` inside their folders.

### 2. Running the DB

We run the MongoDB using `docker`, make sure you have `docker` installed or install it [here](https://www.docker.com/)

#### For Mac Users

You can just run the `run.sh` file and the MongoBD will start shortly

#### For Windows Users

Then you need to open the folder `DB/run.sh` and change the mount path to the Full Folder path of the folder `db-volume`.

For exmaple, after the change the `run.sh` file will look like this:

```
docker run -d --name mongo -p 27017:27017 -v "C:\Users\David\Developer\EX2\DB\db-volume":/data/db mongo
```

### 3. Run the Server

Enter the `Server` folder and run `npm start`

### 4. Run the Client

Enter the `Client` folder and run `npm start`
