import mongoose from "mongoose";

const Schema = mongoose.Schema;

const Item = new Schema({
  title: String,
  description: String,
  price: Number,
  imageURL: String,
});

export default mongoose.model("Item", Item);
