import mongoose from "mongoose";

const Schema = mongoose.Schema;

// this a diffrent model cause in the future this wil be link to the user
const Purchase = new Schema({
  title: String,
  description: String,
  price: Number,
  imageURL: String,
});

export default mongoose.model("Purchase", Purchase);
