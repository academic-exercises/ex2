import * as winston from "winston";
import fs from "fs";

const colors = {
  error: "red",
  warn: "yellow",
  info: "green",
  http: "magenta",
  debug: "white",
};

winston.addColors(colors);

const format = winston.format.combine(
  winston.format.timestamp({ format: "YYYY-MM-DD HH:mm:ss:ms" }),
  winston.format.printf(
    (info) => `${info.timestamp} ${info.level}: ${info.message}`
  )
);

const logListeners = [];

export enum LogType {
  info = "info",
  warning = "warning",
  error = "error",
  http = "http",
}

const createLoggerByType = (logType: LogType) => {
  const filePath = `./server-logs/${logType}.log`;

  return winston.createLogger({
    format: format,
    level: logType,
    defaultMeta: { service: "user-service" },
    transports: [
      new winston.transports.Console(),
      new winston.transports.File({
        filename: filePath,
        level: logType,
        maxsize: 10000000, // 10MB
      }),

      new winston.transports.File({ filename: "./server-logs/combined.log" }),
    ],
  });
};

const createLogger = () => {
  let newLogger = {};

  fs.rmSync("./server-logs", { recursive: true, force: true });

  for (let type in LogType) {
    if (isNaN(Number(type))) {
      const currentLogType: LogType = LogType[type];
      newLogger[currentLogType] = (msg) => {
        logListeners.forEach((logListener) => logListener(msg, type));
        createLoggerByType(currentLogType)[currentLogType](msg);
      };
    }
  }
  return newLogger;
};

const Logger: any = createLogger();
Logger.setOnLog = (newOnLogEvent: (log: any, logType: LogType) => void) => {
  logListeners.push(newOnLogEvent);
};

Logger.info("Server Start Up");

export class httpRequestStream {
  write(text: string) {
    if (text && text !== "") {
      Logger.http(text.replace("\n", ""));
    }
  }
}

export default Logger;
