import mongoose from "mongoose";
import { envConfig } from "./envConfig";
import Logger from "./Logger";

const connectToDB = async () => {
  try {
    await mongoose.connect(envConfig.MONGO_URI, {});
    Logger.info("Connected to MongoDB");
  } catch (error) {
    console.log(error);
  }
};

const DBManager = {
  connectToDB,
};

export default DBManager;
