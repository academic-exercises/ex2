export const envConfig = {
  SERVER_PORT: parseInt(process.env.SERVER_PORT!),
  CORS_WHITELIST: JSON.parse(process.env.CORS_WHITELIST!),
  MONGO_URI: process.env.MONGO_URI!,
};

console.log(envConfig);
// check if all env variables are set
const envConfigKeys = Object.keys(envConfig);
for (const envConfigKey of envConfigKeys) {
  if (envConfig[envConfigKey] === undefined) {
    throw new Error(`Missing env variable: ${envConfigKey}`);
  }
}
