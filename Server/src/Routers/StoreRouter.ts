import express from "express";
import Item from "../Models/Item";

const StoreRouter = express.Router();

StoreRouter.get("/", async (req, res) => {
  Item.find({}, (err, items) => {
    if (err) {
      res.send(err);
    } else {
      res.send(items);
    }
  });
});

StoreRouter.post("/", async (req, res) => {
  const item = new Item(req.body);
  item.save((err, item) => {
    if (err) {
      res.send(err);
    }
    res.send(item);
  });
});

StoreRouter.delete("/", async (req, res) => {
  Item.deleteOne({ _id: req.body._id }, (err, item) => {
    if (err) {
      res.send(err);
    }
    res.send(item);
  });
});

StoreRouter.put("/", async (req, res) => {
  Item.updateOne({ _id: req.body._id }, req.body, (err, item) => {
    if (err) {
      res.send(err);
    }
    res.send(item);
  });
});

export default StoreRouter;
