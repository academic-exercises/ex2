import express from "express";
import Item from "../Models/Item";
import Purchase from "../Models/Purchase";

const CartRouter = express.Router();

CartRouter.get("/", async (req, res) => {
  Purchase.find({}, (err, purchases) => {
    if (err) {
      res.send(err);
    }
    res.send(purchases);
  });
});

const savePurchasePromise = (purchase) => {
  return new Promise((resolve, reject) => {
    purchase.save((err, purchase) => {
      if (err) {
        reject(err);
      }
      resolve(purchase);
    });
  });
};

CartRouter.post("/", async (req, res) => {
  const purchases = req.body;
  for (const purchase of purchases) {
    const purchaseModel = new Purchase(purchase);
    await savePurchasePromise(purchaseModel);
  }

  res.send("Done");
});

export default CartRouter;
