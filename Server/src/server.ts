import Logger, { httpRequestStream } from "./Utils/Logger";
import "dotenv/config";
import express from "express";
import morgan from "morgan";
import cors from "cors";
import { envConfig } from "./Utils/envConfig";
import CartRouter from "./Routers/CartRouter";
import DBManager from "./Utils/DBManager";
import StoreRouter from "./Routers/StoreRouter";

const main = async () => {
  await DBManager.connectToDB();

  const app = express();

  const corsOptions = {
    credentials: true,
    origin: (origin, callback) => {
      if (!origin || envConfig.CORS_WHITELIST.indexOf(origin) !== -1) {
        callback(null, true);
      } else {
        callback(new Error(`Not allowed by CORS - Your Origin: ${origin}`));
      }
    },
  };

  app.use(
    morgan(":method :url :status - :response-time ms", {
      stream: new httpRequestStream(),
    })
  );

  app.use(cors(corsOptions));
  app.use(express.json());

  app.get("/", (req, res) => {
    res.send(`Server is healthy - Uptime: ${process.uptime()} seconds`);
  });

  app.get("/health", (req, res) => {
    res.send(`Server is healthy - Uptime: ${process.uptime()} seconds`);
  });

  app.use("/cart", CartRouter);
  app.use("/store", StoreRouter);

  app.listen(envConfig.SERVER_PORT, () => {
    Logger.info(
      `Cart Server - listening at http://localhost:${envConfig.SERVER_PORT}`
    );
  });
};

main();

process.on("uncaughtException", (err) => {
  Logger.error("Caught exception: " + err);
});

process.on("SIGINT", function () {
  process.exit(1);
});
